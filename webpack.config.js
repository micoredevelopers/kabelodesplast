const path = require('path')
const JsPlugins = require('./webpack/Js')
const CssPlugins = require('./webpack/Css')
const HtmlPlugins = require('./webpack/Html')
const SassPlugins = require('./webpack/Sass')
const CopyPlugins = require('./webpack/Copy')
const CleanPlugins = require('./webpack/Clean')
// const TerserPlugins = require('./webpack/Terser')

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  output: { filename: './js/bundle.js' },
  entry: ['./src/js/index.js', './src/scss/style.scss'],
  module: {
    rules: [
      {
        test: /\.html$/,
        include: path.resolve(__dirname, './src/html/includes'),
        use: ['raw-loader']
      },
      JsPlugins,
      SassPlugins
    ]
  },
  // optimization: { minimizer: [TerserPlugins()] },
  plugins: [CleanPlugins(), CssPlugins(), CopyPlugins(), ...HtmlPlugins()]
}
