require('./functions')

const main = document.querySelector('main')
const pageId = main.getAttribute('id')

switch (pageId) {
  case 'main-page':
    require('./pages/mainPage')
    break
  case 'products-page':
    require('./pages/productsPage')
    break
  case 'selected-product-page':
    require('./pages/productPage')
    break
  case 'certificates-page':
    require('./pages/certificatesPage')
    break
  case 'about-page':
    require('./pages/aboutPage')
    break
}
