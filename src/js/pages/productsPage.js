function clearFilters() {
  $('#filter-form').find('input[type=hidden]').val('')
  $('#filter-form .filter-box__checkbox').prop('checked', false)
  $('#search-input').val('')
  $('.count-item').removeClass('active')
}

$(document).ready(function () {
  let cutVals = []
  let typeVals = []

  $('.count-item').click(function () {
    const val = $(this).attr('data-count')

    $('.count-item').removeClass('active')
    $(this).addClass('active')

    $('#count-input').val(val)
  })

  $('.type-filter .filter-box__checkbox').on('change', function () {
    const val = $(this).attr('data-type')
    const index = typeVals.indexOf(val)

    if ($(this).prop('checked')) {
      typeVals.push(val)
    } else {
      if (index !== -1) typeVals.splice(index, 1)
    }

    $('#type-input').val(typeVals)
  })

  $('.cut-filter .filter-box__checkbox').on('change', function () {
    const val = $(this).attr('data-cut')
    const index = cutVals.indexOf(val)

    if ($(this).prop('checked')) {
      cutVals.push(val)
    } else {
      if (index !== -1) cutVals.splice(index, 1)
    }

    $('#cut-input').val(cutVals)
  })

  $('.modal-body__clear-btn').on('click', function () {
    clearFilters()
  })
})
