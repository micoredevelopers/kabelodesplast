function initMainSlider() {
  const slider = $('.main-slider-section .slider')

  slider.slick({
    dots: true,
    infinite: true,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '.main-slider-section .slider-navigation__arrow-prev',
    nextArrow: '.main-slider-section .slider-navigation__arrow-next',
  })
}

$(document).ready(function () {
  initMainSlider()
})
