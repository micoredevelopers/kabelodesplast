$(document).ready(function () {
  const yearSelect = $('#year-select')
  const clearBtn = $('.certificates-section__clear-btn')

  yearSelect.on('change', function () {
    const val = $(this).val()

    $.ajax({ url: `some_url?year=${val}`, method: 'GET' })
      .done(function (res) {
        console.log(res)
      })
  })

  clearBtn.on('click', function () {
    yearSelect.val('all')

    $.ajax({ url: `some_url?year=all`, method: 'GET' })
      .done(function (res) {
        console.log(res)
      })
  })
})