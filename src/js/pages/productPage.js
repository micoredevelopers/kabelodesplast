function initProductSlider() {
  const slider = $('.product-slider')

  slider.slick({
    dots: false,
    infinite: true,
    speed: 700,
    arrows: false,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: false,
    swipe: false,
    asNavFor: '.product-small-slider',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          dots: true,
          draggable: true,
          fade: false,
          swipe: true
        }
      }
    ]
  })
}

function initProductSmallSlider() {
  const sliderSmall = $('.product-small-slider')

  sliderSmall.slick({
    dots: false,
    speed: 700,
    arrows: false,
    focusOnSelect: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.product-slider',
    vertical: true,
    verticalSwiping: true,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          vertical: false,
          verticalSwiping: false,
        }
      }
    ]
  })
}

$(document).ready(function () {
  initProductSlider()

  if (window.innerWidth > 991) {
    initProductSmallSlider()
  }
})