$(document).ready(function () {
  const aboutSlider = $('.about-slider')

  aboutSlider.slick({
    speed: 700,
    dots: false,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  })
})
