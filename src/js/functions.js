function orderSubmit(form) {
  let data = {}
  const url = form.attr('action')
  const inputs = form.find('input')

  inputs.map((index, input) => {
    const val = $(input).val()
    const name = $(input).attr('name')

    data[name] = val
  })

  setTimeout(function () {
    successSend()
  }, 500)

  // $.ajax({ url, data, method: 'POST' }).done(function (res) {
  //   console.log(res)
  // })
}

function successSend() {
  $('#modalOrder').modal('hide')
  $('#modalOrder').find('input').val('')
  $('#modalSuccess').modal('show')
}

$(document).ready(function () {
  $('input[type=tel]').mask('+38 (099) 999 99 99')

  $('.header__menu-box').click(function () {
    $('.header').toggleClass('open')
    $('html, body').toggleClass('overflow-hidden')
  })
  $('.right-nav-menu__menu-icon').click(function () {
    $('.right-nav-menu').toggleClass('collapsed')
  })

  $('#formOrder').on('submit', function (e) {
    e.preventDefault()
    orderSubmit($(this))
  })
})
